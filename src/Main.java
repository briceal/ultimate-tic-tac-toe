import java.util.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Player {

    private static int OPPONENT = 2;
    private static int PLAYER = 1;

    public static void main(String args[]) {
        Random random = new Random();
        Scanner in = new Scanner(System.in);
        int[][] board = new int[9][9];

        // game loop
        while (true) {
            String move = doNextMove(in, random, board);
            System.out.println(move);
        }
    }

    public static String doNextMove(Scanner in, Random random, int[][] board) {
        int opponentRow = in.nextInt();
        int opponentCol = in.nextInt();

        if (opponentCol != -1 && opponentRow != -1) {
            board[opponentRow][opponentCol] = OPPONENT;
        }

        int validActionCount = in.nextInt();
        List<String> validActions = new ArrayList<>(validActionCount);
        for (int i = 0; i < validActionCount; i++) {
            int row = in.nextInt();
            int col = in.nextInt();

            validActions.add(createAction(row, col));
        }

        String move = null;
        for (String action : validActions) {
            int[][] nextBoard = createStateFromAction(PLAYER, action, board);
            if (get9x9Winner(nextBoard) == PLAYER) {
                move = action;
                break;
            }
        }

        if (move == null) {
            move = validActions.get(random.nextInt(validActions.size()));
        }

        // TODO: DRY this up
        String[] actions = move.split(" ");
        int actionRow = Integer.parseInt(actions[0]);
        int actionCol = Integer.parseInt(actions[1]);
        board[actionRow][actionCol] = PLAYER;

        return move;
    }

    private static String createAction(int row, int col) {
        return String.format("%s %s", row, col);
    }

    public static int[][] createStateFromAction(int player, String action, int[][] board) {
        if (action == null || board == null) {
            throw new RuntimeException("null arguments not allowed");
        }

        String[] actions = action.split(" ");
        if (actions.length != 2) {
            throw new RuntimeException("incorrect format for action, expecting two space separated ints, got: " + action);
        }

        int actionRow;
        int actionCol;
        try {
            actionRow = Integer.parseInt(actions[0]);
            actionCol = Integer.parseInt(actions[1]);
        } catch (NumberFormatException nfe) {
            throw new RuntimeException("Error while parsing supplied integers", nfe);
        }

        if (board[actionRow][actionCol] != 0) {
            throw new RuntimeException("board is already populated at cell: " + action);
        }

        int[][] clone = new int[board.length][board[0].length];
        for (int row = 0; row < board.length; row++) {
            for (int col = 0; col < board[0].length; col++) {
                clone[row][col] = board[row][col];
            }
        }

        clone[actionRow][actionCol] = player;
        return clone;
    }

    public static int get3x3Winner(int[][] board, int r, int c) {
        for (int i = 0; i < 3; i++) {
            if (board[r + 0][c + i] != 0
                    && board[r + 0][c + i] == board[r + 1][c + i]
                    && board[r + 0][c + i] == board[r + 2][c + i])
                return board[r + 0][c + i];
            if (board[r + i][c + 0] != 0
                    && board[r + i][c + 0] == board[r + i][c + 1]
                    && board[r + i][c + 0] == board[r + i][c + 2])
                return board[r + i][c + 0];
        }

        if (board[r + 0][c + 0] != 0 && board[r + 0][c + 0] == board[r + 1][c + 1] && board[r + 0][c + 0] == board[r + 2][c + 2])
            return board[r + 0][c + 0];
        if (board[r + 2][c + 0] != 0 && board[r + 2][c + 0] == board[r + 1][c + 1] && board[r + 2][c + 0] == board[r + 0][c + 2])
            return board[r + 2][c + 0];

        return 0;
    }

    public static int get9x9Winner(int[][] board) {
        int[][] collapsed = new int[3][3];
        for (int r = 0; r < 3; r++)
            for (int c = 0; c < 3; c++)
                collapsed[r][c] = get3x3Winner(board, r * 3, c * 3);

        int winner = get3x3Winner(collapsed, 0, 0);

        if (winner == 0) {
            int p1Wins = 0;
            int p2Wins = 0;
            for (int r = 0; r < 3; r++)
                for (int c = 0; c < 3; c++) {
                    int outcome = collapsed[r][c];
                    if (outcome == PLAYER)
                        p1Wins++;
                    else if (outcome == OPPONENT)
                        p2Wins++;
                    else {
                        for (int i = r / 3; i < r / 3 + 3; i++) {
                            for (int j = c / 3; j < c / 3 + 3; j++) {
                                if (board[i][j] == 0)
                                    return 0;
                            }
                        }
                    }
                }
            return p1Wins > p2Wins ? PLAYER : OPPONENT;
        }

        return winner;
    }
    public static List<String> getValidAction(int[][] board, int lastPlayer, int lastPlayRow, int lastPlayCol)
    {
        Scanner in = new Scanner(System.in);
        int validActionCount = in.nextInt();
        List<String> actions = new ArrayList<>(validActionCount);
        for (int i = 0; i < validActionCount; i++) {
            int row = in.nextInt();
            int col = in.nextInt();
            actions.add(createAction(row, col));
        }
        int currentPlayer = 0;
        if(lastPlayer == 1) {
            currentPlayer = 2;
            board[lastPlayRow][lastPlayCol] = currentPlayer;
        }
        else {
            currentPlayer = 1;
            get9x9Winner(board);
        }

        // get all of the valid actions and pick the best one that will lead to the winning board. Opponent has to play in the exact same spot of the board

        return actions;
    }
}