import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.util.Random;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetNextMoveTests {
    private Scanner buildScanner(int opponentRow, int opponentCol, String... actions) {
        StringBuilder builder = new StringBuilder();
        builder.append(Integer.toString(opponentRow));
        builder.append("\n");
        builder.append(Integer.toString(opponentCol));
        builder.append("\n");
        builder.append(Integer.toString(actions.length));
        builder.append("\n");
        for (String action : actions) {
            builder.append(action);
            builder.append("\n");
        }

        return new Scanner(new ByteArrayInputStream(builder.toString().getBytes()));
    }

    @Test
    void randomMoveIsSelectedForNonWinningBoard() {
        int[][] board = new int[][]{
                {0, 0, 1, 0, 0, 0, 0, 0, 0},
                {2, 2, 0, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
        };
        Scanner in = buildScanner(-1, -1,
                "0 0",
                "0 1",
                "1 2",
                "2 1",
                "2 2");
        Random rnd = new Random(100);
        String output = Player.doNextMove(in, rnd, board);

        assertEquals("0 0", output);
    }

    @Test
    void boardReflectsGameStateAfterMoveIsSelected() {
        int[][] board = {
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 0, 0, 0, 0, 0},
        };

        Scanner in = buildScanner(1, 1, "0 0");
        Random rnd = new Random(100);
        Player.doNextMove(in, rnd, board);

        assertEquals(2, board[1][1]);
        assertEquals(1, board[0][0]);
    }

    @Test
    void winningMoveIsSelectedIfOneMoveAway() {
        int[][] board = new int[][]{
                {0, 0, 1, 0, 0, 0, 0, 0, 0},
                {2, 0, 1, 0, 0, 0, 0, 0, 0},
                {1, 0, 0, 0, 0, 0, 0, 0, 0},
                {1, 0, 2, 0, 0, 0, 0, 0, 0},
                {0, 1, 2, 0, 0, 0, 0, 0, 0},
                {2, 0, 1, 0, 0, 0, 0, 0, 0},
                {1, 0, 1, 0, 0, 0, 0, 0, 0},
                {1, 2, 2, 0, 0, 0, 0, 0, 0},
                {1, 0, 2, 0, 0, 0, 0, 0, 0},
        };
        Scanner in = buildScanner(-1, -1,
                "0 0",
                "0 1",
                "2 1",
                "2 2");
        Random rnd = new Random(100);
        String output = Player.doNextMove(in, rnd, board);

        assertEquals("2 2", output);
    }
}
